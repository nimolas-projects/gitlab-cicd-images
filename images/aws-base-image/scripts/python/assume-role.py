import json
import boto3
from sys import argv
from os import environ


def get_aws_client(resource_name):
    return boto3.client(resource_name)


def _get_ssm_param(client, param_name):
    return client.get_parameter(Name=param_name).get("Parameter").get("Value")


def get_account_id(client, account_name):
    account_map = _get_ssm_param(client, "/nimolas-projects/AccountMap")
    account_map = json.loads(account_map)

    return account_map.get(account_name)


def generate_role_and_session_name(account_id):
    pipeline_id = environ["CI_PIPELINE_ID"]
    job_id = environ["CI_JOB_ID"]
    project_name = environ["CI_PROJECT_NAME"]

    return {
        "role": f"arn:aws:iam::{account_id}:role/GitLabCIDeploy",
        "sessionName": f"GitLabCIDeploy-{project_name}-{pipeline_id}--{job_id}"[:64],
    }


if __name__ == "__main__":
    account_name = argv[1]

    ssm_client = get_aws_client("ssm")

    account_id = get_account_id(ssm_client, account_name)

    role_and_session = generate_role_and_session_name(account_id)

    print(json.dumps(role_and_session, default=str))
