#!/bin/bash

role_and_session=$(python /usr/local/bin/assume-role.py $1)

role=$(echo $role_and_session | jq -r .role)
session_name=$(echo $role_and_session | jq -r .sessionName)

KST=$(aws sts assume-role --role-arn $role --role-session-name $session_name --output json)

AWS_ACCESS_KEY_ID=$(echo $KST | jq -r .Credentials.AccessKeyId)
AWS_SECRET_ACCESS_KEY=$(echo $KST | jq -r .Credentials.SecretAccessKey)
AWS_SESSION_TOKEN=$(echo $KST | jq -r .Credentials.SessionToken)
AWS_SECURITY_TOKEN=$(echo $KST | jq -r .Credentials.SessionToken)

aws configure set profile.ci.region "eu-west-2"
aws configure set profile.ci.aws_access_key_id $AWS_ACCESS_KEY_ID
aws configure set profile.ci.aws_secret_access_key $AWS_SECRET_ACCESS_KEY
aws configure set profile.ci.aws_session_token $AWS_SESSION_TOKEN
aws configure set profile.ci.aws_security_token $AWS_SECURITY_TOKEN